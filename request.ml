class request (path : string) = object (self)
    val path = path
    method path = path
end

let r = new request "/"


let () =
    print_endline r#path
