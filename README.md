# ocaml

ML functional language with OO extensions. https://ocaml.org/

## Official documentation
* [OCaml User’s manual](http://caml.inria.fr/pub/docs/manual-ocaml/)
* [Using opam](https://opam.ocaml.org/doc/Usage.html)
* [Install OCaml](https://ocaml.org/docs/install.html)

## Unofficial documentation
* repology:
    [ocaml](https://repology.org/project/ocaml/versions)
    [opam](https://repology.org/project/opam/versions)
* [*OCaml/Introduction*](https://en.wikiversity.org/wiki/OCaml/Introduction)
  ([fr](https://fr.wikiversity.org/wiki/Premiers_pas_en_OCaml))